"use strict"

// 1. Метод forEach запускает (перебор) функцию для каждого єлемента массива.
// 2. [массив].length = 0, [массив].splice(0).
// 3. Array.isArray(value), возвращает true, если value является массивом, иначе false.


function filterBy(array, data) {
    
    const someArray = array.filter (item => (typeof item !== typeof data));

    return someArray;
}

const newArray = ['hello', 'world', 23, '23', null, true];

console.log(filterBy(newArray, 'string'));